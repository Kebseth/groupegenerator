import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class GroupeManager {
    private String numberOfGroup;
    private List<Personne> list;

    public List<Personne> getList() {
        return list;
    }

    public GroupeManager(List<Personne> list, String numberOfGroup){
        this.numberOfGroup = numberOfGroup;
        this.list = list;
    }

    public String getNumberOfGroup() {
        return numberOfGroup;
    }

    public boolean verifyInput() {
        try {
            Integer.valueOf(this.numberOfGroup);
        } catch (Exception e) {
            return false;
        }
        int input = Integer.valueOf(this.numberOfGroup);
        if(input > this.list.size() || input < 2){
            return false;
        } else return true;
    }

    public List<Groupe> algo(){

        List<Groupe> listeOfGroups = new ArrayList<>();

        // Randomize
        Collections.shuffle(this.list);

        if(this.verifyInput()){
            // Conversion
            int number = (Integer.valueOf(numberOfGroup)); // -> 3

            // Modulo
            int modulo = this.list.size() % number; // -> 0

            // Nombre de personne par groupe
            double numberOfPersonne = this.getList().size() / number;

            // Nombre de personne par groupe INTEGER
            int roundNumber = Integer.valueOf((int) Math.ceil(numberOfPersonne));
           

            // Creation du nombre de groupe
            for (int i = 0; i < number; ++i){
                Groupe test = new Groupe();
                listeOfGroups.add(test);
            }

            for (int i = 0; i < this.getList().size(); ++i){
                //listeOfGroups.get(0).addPersonne(list.get(i));
                for(int j = 0; j < listeOfGroups.size(); ++j){
                    if(listeOfGroups.get(j).getList().size() < roundNumber ) {
                        listeOfGroups.get(j).addPersonne(this.getList().get(i));
                    }
                }
            }

            return listeOfGroups;
        } else {
            return listeOfGroups;
        }

    }
}
