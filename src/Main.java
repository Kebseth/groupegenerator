import java.util.List;

public class Main {
    public static void main(String[] args) {
        Groupe stagiaires = new Groupe();
        List<String> stagiairesList = List.of("P.A","JO", "Baptiste", "Maxime", "Sarah", "Stéphane", "Erwan", "Richard", "Marion", "Aurelien", "Charles", "Cynthia");

        for (String p:stagiairesList) {
            Personne pers = new Personne(p);
            stagiaires.addPersonne(pers);
        }

        GroupeManager manager = new GroupeManager(stagiaires.getList(), "3");

        manager.algo();

        /* for (int i=0; i < stagiaires.getList().size(); ++i) {
            System.out.println(stagiaires.getList().get(i).getName());
        } */
        //System.out.println("\n\n -------------");
        List<Personne> g1 = manager.algo().get(0).getList();
        List<Personne> g2 = manager.algo().get(1).getList();
        List<Personne> g3 = manager.algo().get(2).getList();
        manager.algo();

        System.out.println(g1.size() + "/12  Groupe 1:\n");
        System.out.println(g1.get(0).getName());
        System.out.println(g1.get(1).getName());
        System.out.println(g1.get(2).getName());
        System.out.println(g1.get(3).getName());
        System.out.println("\n" + g2.size() + "/12  Groupe 2:\n");
        System.out.println(g2.get(0).getName());
        System.out.println(g2.get(1).getName());
        System.out.println(g2.get(2).getName());
        System.out.println(g2.get(3).getName());
        System.out.println("\n" + g3.size() + "/12  Groupe 3:\n");
        System.out.println(g3.get(0).getName());
        System.out.println(g3.get(1).getName());
        System.out.println(g3.get(2).getName());
        System.out.println(g3.get(3).getName());
    }
}
