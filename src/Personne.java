public class Personne {
    private String name;

    public Personne(String name){
        this.name = name;
    }

    public String getName(){ return this.name; }

    @Override
    public String toString() {
        return name;
    }
}
