import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import java.util.List;

public class GroupeTest {

    @Test
    void addPersonne(){

        Personne test1 = new Personne("Test 1");
        Personne test2 = new Personne("Test 2");
        Personne test3 = new Personne("Test 3");

        Groupe groupe = new Groupe();

        groupe.addPersonne(test1);
        groupe.addPersonne(test2);
        groupe.addPersonne(test3);

        Assertions.assertEquals(groupe.getList(), List.of(test1, test2, test3));
    }
}
