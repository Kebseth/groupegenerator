import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class PersonneTest {

    @Test
    void getName(){
        Personne pers = new Personne("Test");
        Assertions.assertEquals("Test", pers.getName());
    }
}
