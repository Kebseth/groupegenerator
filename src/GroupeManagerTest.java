import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class GroupeManagerTest {

    public Groupe groupe;


    @BeforeEach
    void setUp(){
        Personne toto = new Personne("erg");
        Personne toto2 = new Personne("ersgs");
        Personne toto3 = new Personne("erhh");
        Personne toto4 = new Personne("ersgs");
        Personne toto5 = new Personne("erfgsg");
        Personne toto6 = new Personne("ersfgsg");
        Groupe groupe = new Groupe();
        groupe.addPersonne(toto);
        groupe.addPersonne(toto2);
        groupe.addPersonne(toto3);
        groupe.addPersonne(toto4);
        groupe.addPersonne(toto5);
        groupe.addPersonne(toto6);
        this.groupe = groupe;
    }


    @Test
    void verifyInputInteger(){
        GroupeManager groupeManager = new GroupeManager(this.groupe.getList(), "2.5");
        Assertions.assertFalse(groupeManager.verifyInput());
    }

    @Test
    void verifyInputInteger2(){

        GroupeManager groupeManager = new GroupeManager(this.groupe.getList(), "toto");
        Assertions.assertFalse(groupeManager.verifyInput());
    }

    @Test
    void verifyInputValid(){

        GroupeManager groupeManager = new GroupeManager(this.groupe.getList(), "3");
        Assertions.assertTrue(groupeManager.verifyInput());
    }

    @Test
    void verifyInputNotExcludeFromList(){

        GroupeManager groupeManager = new GroupeManager(this.groupe.getList(), "1");
        Assertions.assertFalse(groupeManager.verifyInput());
    }

    @Test
    void verifyInputNotExcludeFromList2(){

        GroupeManager groupeManager = new GroupeManager(this.groupe.getList(), "9");
        Assertions.assertFalse(groupeManager.verifyInput());
    }

    @Test
    void isTheNumberOfTheMethodWorking() {
        GroupeManager groupeManager = new GroupeManager(this.groupe.getList(), "3");
        Assertions.assertEquals(3, groupeManager.algo().size());
    }


    @Test
    void isTheNumberOfPersonsIsEquivalentAtTheEnd() {
        GroupeManager groupeManager = new GroupeManager(this.groupe.getList(), "3");
        Assertions.assertEquals(2, groupeManager.algo().get(0).getList().size());
    }
}
